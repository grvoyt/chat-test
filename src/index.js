import {Client as WebSocket} from "rpc-websockets";
import {init} from "./iniciators";
import moment from 'moment'
import _ from 'lodash';

init();

var ws = new WebSocket('ws://localhost:8080/?api=asdadasd');

window.ws = ws

let chats = [];

let $_GET = GET();

const login = $_GET['user'];

window.users = {
    'grvoyt': {
        uuid: 'f9cf705e-fe3f-4b86-8531-ceb490cc2e19',
        token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiMzFhMWE2YTFmNTdhNzI2MGYwZmI5MjY4Y2Q4MWJlNjhjYzhkZTBmMDVhZmJlYWZkODQ4MWI1OGJhMGIwZDM3YjVmZDI0OGNlOTA3MDY1MWYiLCJpYXQiOjE2MDkyNDg0MTAsIm5iZiI6MTYwOTI0ODQxMCwiZXhwIjoxNjQwNzg0NDEwLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.q4gvw0hugLXdnJgaXLpTix1FTSCNqqQ5xeGARhhr_L1-GIFJBe6YIiimG2iD4A_2VKOaCaYANu3boL3deJEVbGlKn3y-yszeFJK6tucLAKZC5nQpVnUYbvhxw11bzh15yho4IPfs-AFTdkEjtnmYLQBh_KFaqWOOoQ-tjyL1b7CR51LYfzGdBpOPd-dljrs15GreeC8XPkoT1k5BHH0tyUdPY4KlogPEUm_IMFWc5wd7Oxo1Eskpj5tRHvZ1vr4BJyQd470fA_eufdz3AdwPmvj8qwx89xmN0fpIiS_q3fTzIpIa_rJ_p3zBainhBGXHNpPaFt-IAFJAcCBUG_YGQeQFLxjEyHZ1RPHe1qI2CqgV1KZBnEN0mxWUvTMz2q0ZupuZQ6nMwqxUSKSBvF1l_SnTDBBwbdqPrrDGW0eukL-GWVVdXqaeQXPNzqgF_CcXiVFdDYQuaMPbgccO0G16cuOKCFtIBb8g8emRVBlZ1nh8YfAJuMJu4figUYnuNdh_k2IY01hmapZ-K1PBLN6Yq6_IlLFKmwUAErhxS0R1LGpNQc46dm00SUJrZgn9-m_fhQZb0r137vWXtSX8N93taxIJwH7FqCrVfazURpvAvEQ60LZggBMdzg8scGNfpDPR9tUghJkaHQlLYzlDV6UJTTGMpzeHLw7BihdLkG_Wczw',
        avatar: 'img/demo/avatars/avatar-b.png',
    },
    'invi': {
        uuid: '6bc69aa3-2eb5-4dfb-9f46-96223321ce8c',
        token: 'ccd06c1fffb67df21a6cb5a2f21022b4',
        avatar: 'img/demo/avatars/avatar-e.png'
    },
    'nikolay': {
        uuid: '1dbc5cc4-5c7a-48cb-85e7-8690e8b4cfeb',
        token: '88b1be54c481a01d495bf04164110ab1',
        avatar: 'img/demo/avatars/avatar-g.png',
    }
}
let to = $_GET['to'];

window.userAuthed = {}
window.chatUsed = {}
window.chatLIst = []

$('#chat_container').empty();

ws.on('open', () => {

    const uuid = users[login].uuid;
    const token = users[login].token;
    // login your client to be able to use protected methods
    ws.login({uuid,token}).then(function(result) {
        ws.call('user.info',[]).then( (user) => {
            userAuthed = {
                ...user,
                avatar: users[login].avatar
            };
            printAuthUser();
        })
        ws.call('chat.info',[]).then(function({chats,users}) {
            chatLIst = chats;
            printUser(users);
        })
        // subscribe to receive an event
        ws.subscribe([
            'event.chat.message'
        ]);

        if( !!to ) {
            let UuidFrom = window.users[login].uuid;
            let UuidTo   = window.users[to].uuid;
            ws.call('chat.make',[UuidFrom,UuidTo]).then(function({chat,messages}) {
                chatUsed = chat;
                printMessages(messages);
            });
        }


        ws.on('event.message', function(message) {
            if( message.chat_id != chatUsed.name ) return ;

            let classChat = message.from == userAuthed.id ? 'chat-segment-sent' : 'chat-segment-get';

            let html = `<div class="chat-segment ${classChat} chat-end">
                            <div class="chat-message">
                                <p>${message.text}</p>
                            </div>
                            <div class="text-right fw-300 text-muted mt-1 fs-xs">
                                ${ moment(message.createdAt).format('h:mm D.MMM') }
                            </div>
                        </div>`
            $('#chat_container').append(html);
        })

        ws.on('event.chat.online', ({uuid,online}) => {
            console.log('event.chat.online',uuid,online)
        })

        //Chack User
        ws.call('user.check',['ping']).then(result => {
            console.log('user.check',result);
        })
        setInterval( () => {
            ws.call('user.check',['ping']).then(result => {
                console.log('user.check',result);
            })
        },30 * 1000)

        //GetLastActivity
        ws.call('user.lastActivity').then( result => {
            console.log('user.lastActivity',result);
        })
        setInterval( () => {
            ws.call('user.lastActivity').then( result => {
                console.log('user.lastActivity',result);
            })
        },60 * 1000);

    }).catch(function(error) {
        console.log('auth failed')
    })


})

$('.sender').on('click',function(e) {
    let text = $('#msgr_input').text();
    let chat_id = chatUsed.name;
    let user_id = userAuthed.id;
    ws.notify('chat.message',[chat_id,user_id,text])

    $('#msgr_input').text('');
});

window.onbeforeunload = function() {
    ws.close();
}

function GET() {
    let query = window.location.search.substr(1);
    let results = [];
    query.split('&').forEach( params => {
        let [key,value] = params.split('=');
        results[key] = value;
    })
    return results;
}

function printUser(users) {
    $('#js-msgr-listfilter').empty();
    let html = '';

    users.forEach( UserItem => {
        let userWithLogin = _.map(window.users, (item,login) => {
            return {...item,login};
        })

        let UserTo = _.find(userWithLogin, item => item.uuid == UserItem.uuid )

        let loginUser = _.find(userWithLogin, item => item.uuid == userAuthed.uuid);

        html += `<li>
                <a href="/?user=${loginUser.login}&to=${UserTo.login}" class="d-table w-100 px-2 py-2 text-dark hover-white" data-filter-tags="oliver kopyuv online">
                    <div class="d-table-cell align-middle status status-success status-sm ">
                        <span class="profile-image-md rounded-circle d-block" style="background-image:url(${UserTo.avatar}); background-size: cover;"></span>
                    </div>
                    <div class="d-table-cell w-100 align-middle pl-2 pr-2">
                        <div class="text-truncate text-truncate-md">
                            ${UserTo.login}
                            <small class="d-block font-italic text-success fs-xs">
                                Online
                            </small>
                        </div>
                    </div>
                </a>
            </li>`
    })


    $('#js-msgr-listfilter').append(html)
}

function printMessages(messages = []) {
    let html = '';



    messages.forEach( message => {
        let classChat = message.from == userAuthed.id ? 'chat-segment-sent' : 'chat-segment-get';
            html += `<div class="chat-segment ${classChat}">
                    <div class="chat-message">
                        <p>${message.text}</p>
                    </div>
                    <div class="text-right fw-300 text-muted mt-1 fs-xs">
                        ${ moment(message.createdAt).format('h:mm D.MMM') }
                    </div>
                </div>`
    })

    $('#chat_container').append(html);
}

function printAuthUser() {
    let html = ''
    $("#card").empty();
    html += `<div class="mr-2 d-inline-block">
                    <span class="rounded-circle profile-image d-block" style="background-image:url(${userAuthed.avatar}); background-size: cover;"></span>
                </div>
                <div class="info-card-text">
                    <a href="javascript:void(0);" class="fs-lg text-truncate text-truncate-lg" data-toggle="dropdown" aria-expanded="false">
                        ${userAuthed.name}
                        <i class="fal fa-angle-down d-inline-block ml-1 fs-md"></i>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item px-3 py-2" href="#">Send Email</a>
                        <a class="dropdown-item px-3 py-2" href="#">Create Appointment</a>
                        <a class="dropdown-item px-3 py-2" href="#">Block User</a>
                    </div>
                    <span class="text-truncate text-truncate-md opacity-80">Online Strategist</span>
                </div>`

    $("#card").append(html);
}
