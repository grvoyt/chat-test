const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    mode: "development",
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'index.bundle.js'
    },
    watch: false,
    optimization: {
        minimize: false
    },
    plugins: [
        new HtmlWebpackPlugin({template: './src/index.html'}),
        new CopyPlugin({
            patterns: [
                { from: "./src/css", to: "css" },
                { from: "./src/img", to: "img" },
                { from: "./src/js", to: "js" },
                { from: "./src/media", to: "media" },
                { from: "./src/webfonts", to: "webfonts" },
            ],
        }),
    ],
    module: {
        rules: [
            { test: /\.css$/, use: 'css-loader'}
        ]
    }
}
